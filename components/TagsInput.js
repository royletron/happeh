import { useState } from "react";
import TagsInput from "react-tagsinput";
import AutosizeInput from "react-input-autosize";
import {
  DEFAULT_BORDER,
  BORDER_RADIUS,
  DEFAULT_FONT_FAMILY
} from "../constants/Styles";
import { FOREGROUND, BACKGROUND } from "../constants/Colors";

const autosizingRenderInput = ({ addTag, ...props }) => {
  let { onChange, value, ...other } = props;
  return (
    <AutosizeInput type="text" onChange={onChange} value={value} {...other} />
  );
};

const renderTag = ({
  tag,
  key,
  disabled,
  onRemove,
  classNameRemove,
  getTagDisplayValue,
  ...props
}) => {
  return (
    <span key={key} {...props}>
      {getTagDisplayValue(tag)}
      {!disabled && <a onClick={() => onRemove(key)}>x</a>}
      <style jsx>
        {`
          span {
            background: ${FOREGROUND};
            color: white;
            padding: 4px 20px 4px 6px;
            margin: 4px;
            border-radius: ${BORDER_RADIUS};
            font-size: 1.5em;
            position: relative;
          }
          a {
            color: white;
            font-size: 20px;
            position: absolute;
            top: 3px;
            right: 5px;
            cursor: pointer;
          }
        `}
      </style>
    </span>
  );
};

export default ({ tags, setTags, placeholder }) => {
  const [focus, setFocus] = useState(false);
  const onFocus = () => setFocus(true);
  const onBlur = () => setFocus(false);
  return (
    <div>
      <TagsInput
        renderInput={autosizingRenderInput}
        renderTag={renderTag}
        value={tags}
        onChange={setTags}
        inputProps={{ onFocus, onBlur, placeholder }}
      />
      <style jsx>
        {`
          div {
            font-family: ${DEFAULT_FONT_FAMILY};
            font-weight: 200;
            border: ${DEFAULT_BORDER};
            border-radius: ${BORDER_RADIUS};
            padding: 0.8em 1em;
            background-color: #fff;
            transition: all 0.5s;
            margin-bottom: 10px;
            text-align: center;
            min-height: 40px;
            font-size: 1.5em;
            margin-bottom: 20px;
            ${focus && `border-color: ${FOREGROUND};`}
          }
          div :global(input[type="text"]) {
            padding: 0;
            border: none;
            margin-bottom: 0;
            text-align: left;
            width: 160px;
            padding-left: 5px;
          }
        `}
      </style>
    </div>
  );
};
