import { DANGER, FOREGROUND } from "../constants/Colors";
import Link from "next/link";

export default ({ error }) => {
  if (error) {
    return (
      <div>
        {error}.&nbsp;
        <Link href="/">
          <a>Head home</a>
        </Link>
        <style jsx>
          {`
            div {
              background: ${FOREGROUND};
              color: white;
              padding: 5px;
              border-radius: 5px;
              margin-bottom: 20px;
            }
            a,
            a:visited {
              color: white;
              font-weight: bold;
            }
          `}
        </style>
      </div>
    );
  }
  return "";
};
