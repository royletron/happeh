import { BACKGROUND, PRIMARY_BG, PRIMARY_FG } from "../constants/Colors";
import { BORDER_RADIUS } from "../constants/Styles";
import Spinner from "./Spinner";

export const Header = ({ children }) => (
  <header>
    {children}
    <style jsx>
      {`
        header {
          padding: 20px;
          font-weight: bold;
          background: ${PRIMARY_BG};
          color: ${PRIMARY_FG};
        }
      `}
    </style>
  </header>
);

export const Body = ({ children }) => (
  <section>
    {children}
    <style jsx>
      {`
        section {
          padding: 20px;
          background: ${BACKGROUND};
        }
      `}
    </style>
  </section>
);

export default ({ onClose, loading = false, children, ...props }) => (
  <div className="holder" {...props}>
    <div className="close">
      <a onClick={onClose}>X</a>
    </div>
    <article>
      <div className="content">{children}</div>
      {loading && (
        <div className="loading">
          <Spinner />
        </div>
      )}
    </article>
    <style jsx>
      {`
        div.close {
          position: absolute;
          top: 25px;
          right: 25px;
          color: ${BACKGROUND};
        }
        article {
          position: relative;
          width: 600px;
        }
        div.content :global(:first-child) {
          border-top-left-radius: ${BORDER_RADIUS};
          border-top-right-radius: ${BORDER_RADIUS};
        }
        div.content :global(:last-child) {
          border-bottom-left-radius: ${BORDER_RADIUS};
          border-bottom-right-radius: ${BORDER_RADIUS};
        }
        div.holder {
          position: fixed;
          top: 0px;
          left: 0px;
          right: 0px;
          bottom: 0px;
          background: rgba(0, 0, 0, 0.8);
          z-index: 200;
          display: flex;
          flex-direction: column;
          align-items: center;
          justify-content: center;
        }
        div.loading {
          position: absolute;
          top: 0px;
          right: 0px;
          left: 0px;
          bottom: 0px;
          background: rgba(255, 255, 255, 0.2);
          border-radius: ${BORDER_RADIUS};
          display: flex;
          flex-direction: column;
          align-items: center;
          justify-content: center;
        }
      `}
    </style>
  </div>
);
