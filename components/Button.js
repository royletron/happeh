import { Fragment } from "react";
import css from "styled-jsx/css";
import { FOREGROUND, SECONDARY_BG, DANGER, WARNING } from "../constants/Colors";
import {
  BORDER_RADIUS,
  DEFAULT_BORDER,
  HEADER_FONT_FAMILY
} from "../constants/Styles";
import classnames from "classnames";

export const ButtonStyles = css`
  a,
  input[type="submit"] {
    padding: 0.8em 1em;
    background: white;
    color: ${FOREGROUND};
    font-family: ${HEADER_FONT_FAMILY};
    border-radius: ${BORDER_RADIUS};
    cursor: pointer;
    box-shadow: 0em 0em 0px rgba(0, 0, 0, 0.2);
    transition: all 0.5s;
    border: ${DEFAULT_BORDER};
    border-color: rgba(0, 0, 0, 0.2);
    font-weight: 500;
  }
  a.secondary {
    background: ${SECONDARY_BG};
    color: white;
  }
  a.danger {
    background: ${DANGER};
    color: white;
  }
  a.warn {
    background: ${WARNING};
  }
  a.disabled {
    opacity: 0.5;
    pointer-events: none;
    cursor: default;
  }
  a.hero {
    border-color: ${FOREGROUND};
    font-size: 1.6em;
    box-shadow: 0px 0px 40px rgba(0, 0, 0, 0.5);
  }
  input[type="submit"] {
    background: ${SECONDARY_BG};
    color: white;
  }
  a:hover,
  input[type="submit"]:hover {
    text-decoration: underline;
    border-color: ${FOREGROUND};
  }
`;

export default React.forwardRef(
  ({ secondary, danger, warn, disabled, hero, children, ...props }, ref) => (
    <a
      ref={ref}
      className={classnames({ secondary, danger, warn, disabled, hero })}
      {...props}
    >
      {children}
      <style jsx>{ButtonStyles}</style>
    </a>
  )
);

export const Normal = ({
  secondary,
  danger,
  warn,
  disabled,
  children,
  ...props
}) => (
  <a className={classnames({ secondary, danger, warn, disabled })} {...props}>
    {children}
    <style jsx>{ButtonStyles}</style>
  </a>
);

export const Submit = ({ children, ...props }) => (
  <Fragment>
    <input value={children} type="submit" {...props}></input>
    <style jsx>{ButtonStyles}</style>
  </Fragment>
);
