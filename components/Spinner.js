export default ({ text }) => {
  return (
    <div>
      <img src="/imgs/emoji/cookie.svg" />
      {text && <p>{text}</p>}
      <style jsx>
        {`
          img {
            animation: spin 1s infinite;
            height: 180px;
          }
          @keyframes spin {
            from {
              transform: rotate(0deg);
            }
            to {
              transform: rotate(-360deg);
            }
          }
        `}
      </style>
    </div>
  );
};
