import Head from "next/head";
import { PRIMARY_BG } from "../constants/Colors";

export default () => (
  <Head>
    <meta name="viewport" content="width=device-width, initial-scale=1" />
    <title>Happeh - unlock the key that makes you happy.</title>
    <meta charSet="utf-8" />
    <link
      href="https://fonts.googleapis.com/css?family=Josefin+Sans:700|Varela+Round&display=swap"
      rel="stylesheet"
    />
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png" />
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png" />
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png" />
    <link rel="manifest" href="/site.webmanifest" />
    <meta property="og:url" content="https://happeh.dev" />
    <meta property="og:title" content="Unlock the key that makes you happy." />
    <meta
      property="og:description"
      content="There's little time in the day to stop & reflect. HAPPEH gives you time to stop, pause & reflect. It will help you unlock the key that makes you happy."
    />
    <meta property="og:image" content="https://happeh.dev/meta.png" />
    <meta name="theme-color" content={PRIMARY_BG}></meta>
  </Head>
);
