export default ({ size = 50, color = "transparent", children, ...props }) => {
  return (
    <div {...props}>
      {children}
      <style jsx>
        {`
          div {
            background: ${color};
            width: ${size}px;
            height: ${size}px;
            color: white;
            cursor: pointer;
            border-radius: 50%;
            box-shadow: 0px 0px 0px rgba(0, 0, 0, 0);
            transition: all 0.5s;
            overflow: hidden;
            display: flex;
            flex-direction: row;
            align-items: center;
            justify-content: center;
          }
          div > :global(img) {
            max-width: 100%;
          }
          div:hover {
            box-shadow: 4px 4px 0px rgba(0, 0, 0, 0.3);
          }
        `}
      </style>
    </div>
  );
};
