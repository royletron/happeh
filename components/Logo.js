import Link from "next/link";

export default () => (
  <header>
    <Link href="/">
      <a>
        <h1>HAPPEH</h1>
      </a>
    </Link>
    <div>
      <img src="/imgs/emoji/happy.svg" />
      <img src="/imgs/emoji/sad.svg" />
      <img src="/imgs/emoji/distraught.svg" />
    </div>
    <style jsx>
      {`
        a {
          text-decoration: none;
        }
        header {
          margin-bottom: 0.67em;
        }
        h1 {
          text-transform: uppercase;
          font-size: 4em;
          margin-bottom: 0;
        }
        div {
          display: flex;
          flex-direction: row;
          justify-content: center;
        }
        img {
          max-height: 50px;
        }
      `}
    </style>
  </header>
);
