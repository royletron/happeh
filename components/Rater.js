import { useState, useMemo } from "react";
import { useRouter } from "next/router";

import { functions } from "../constants/firebase";

import { geolocated } from "react-geolocated";
import Form from "../components/Form";
import Selector, { Option } from "../components/Selector";
import Spinner from "./Spinner";
import Error from "./Error";
import { Submit, Normal } from "./Button";
import TagsInput from "./TagsInput";

const Rater = ({ isGeolocationAvailable, isGeolocationEnabled, coords }) => {
  const [selected, setSelected] = useState(0);
  const [hardWeek, setHardWeek] = useState(false);
  const [loading, setLoading] = useState(false);
  const [thanks, setThanks] = useState([]);
  const [error, setError] = useState(false);
  const [overrideLocation, setOverrideLocation] = useState(false);
  const onChange = setSelected;
  const router = useRouter();
  const { code } = router.query;
  const onSubmit = evt => {
    evt.preventDefault();
    setLoading(true);
    setError();
    const vote = functions.httpsCallable("httpsVote");
    vote({
      ...(!overrideLocation
        ? {
            lat: coords.latitude,
            long: coords.longitude
          }
        : {}),
      happeh: selected,
      hardWeek,
      thanks,
      code
    })
      .then(() => {
        router.push({
          pathname: "/thanks",
          query: {
            fromCode: code !== undefined
          }
        });
      })
      .catch(err => {
        setError(err.message);
        setLoading(false);
      });
  };

  const onOverride = () => setOverrideLocation(true);
  if (!isGeolocationEnabled && !overrideLocation) {
    return (
      <div>
        <h2>We can do more with your location.</h2>
        <p style={{ marginBottom: 20 }}>
          Accepting the location prompt means you can see how your location
          changes your mood. We understand not everyone wants to share that
          information.
        </p>
        <Normal onClick={onOverride}>Continue without Location.</Normal>
      </div>
    );
  }
  if (loading || (!coords && !overrideLocation)) {
    return (
      <div style={{ display: "contents" }}>
        <Spinner
          text={
            loading
              ? "Saving your rating"
              : !coords
              ? "Getting your location."
              : "Who knows what I am waiting for..."
          }
        />
      </div>
    );
  }
  return (
    <Form onSubmit={onSubmit}>
      <Error error={error} />
      <label>Rate Your Hapiness Today?</label>
      <Selector value={selected} onChange={onChange}>
        <Option value={-2}>
          <img src="/imgs/emoji/distraught.svg" />
        </Option>
        <Option value={-1}>
          <img src="/imgs/emoji/sad.svg" />
        </Option>
        <Option value={0}>
          <img src="/imgs/emoji/ambivalent.svg" />
        </Option>
        <Option value={1}>
          <img src="/imgs/emoji/happy.svg" />
        </Option>
        <Option value={2}>
          <img src="/imgs/emoji/delighted.svg" />
        </Option>
      </Selector>
      <label>Has this week been hard?</label>
      <Selector value={hardWeek} onChange={setHardWeek}>
        <Option value={true}>Yes</Option>
        <Option value={false}>No</Option>
      </Selector>
      <label>What are you thankful for?</label>
      <TagsInput placeholder="Coffee" tags={thanks} setTags={setThanks} />
      <div style={{ width: "100%" }}>
        <Submit type="submit" value="Send"></Submit>
      </div>
      <style jsx>
        {`
          img {
            max-height: 50px;
          }
        `}
      </style>
    </Form>
  );
};

export default geolocated({
  positionOptions: {
    enableHighAccuracy: false
  },
  userDecisionTimeout: 5000
})(Rater);
