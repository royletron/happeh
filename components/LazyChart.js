import dynamic from "next/dynamic";
import Spinner from "./Spinner";

export default dynamic(import("./HappehChart"), {
  loading: () => <Spinner />
});
