import React, { useMemo } from "react";
import classnames from "classnames";
import { FOREGROUND } from "../constants/Colors";
import { HEADER_FONT_FAMILY } from "../constants/Styles";

export const Option = ({
  children,
  onClick,
  value,
  active,
  locked = false,
  ...props
}) => {
  const clicked = () => onClick(value);
  return (
    <div
      className={classnames({ active, locked })}
      onClick={clicked}
      {...props}
    >
      {children}
      <style jsx>
        {`
          div {
            padding: 10px;
            display: flex;
            flex: 1;
            flex-direction: row;
            align-items: center;
            justify-content: center;
            ${!locked ? "cursor: pointer" : "cursor: auto"};
            padding: 10px;
            background: white;
            color: ${FOREGROUND};
            box-shadow: 0em 0em 0px rgba(0, 0, 0, 0.2);
            transition: all 0.5s;
            font-weight: 500;
            font-size: 30px;
            min-height: 50px;
            font-family: ${HEADER_FONT_FAMILY};
            border: 3px solid rgba(0, 0, 0, 0.1);
            border-color: #cccccc;
          }
          div:not(:last-child) {
            border-right: none;
          }
          div:hover {
            text-decoration: underline;
          }
          div:first-child {
            border-top-left-radius: 10px;
            border-bottom-left-radius: 10px;
          }
          div:last-child {
            border-top-right-radius: 10px;
            border-bottom-right-radius: 10px;
          }
          div.active {
            background: ${locked ? "white" : "#cccccc"};
            color: ${FOREGROUND};
            text-decoration: underline;
          }
        `}
      </style>
    </div>
  );
};

export default ({ children, onChange, value, locked = false, ...props }) => {
  let elements = useMemo(() => {
    let arr = React.Children.toArray(children);
    return arr.map(child =>
      React.cloneElement(child, {
        active: value === child.props.value,
        locked,
        onClick: onChange
      })
    );
  }, [children]);
  return (
    <div {...props}>
      {elements}
      <style jsx>
        {`
          div {
            display: flex;
            width: 100%;
            flex-direction: row;
          }

          div:not(:last-child) {
            margin-bottom: 20px;
          }
        `}
      </style>
    </div>
  );
};
