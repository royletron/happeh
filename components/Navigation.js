import { useContext } from "react";
import Link from "next/link";
import UserContext from "../context/User";
import { PRIMARY_BG, PRIMARY_FG } from "../constants/Colors";
import { MAX_WIDTH } from "../constants/Styles";
import ProfileBadge from "./ProfileBadge";

export default () => {
  const { user, profile, loading } = useContext(UserContext);
  return (
    <div>
      <nav>
        <section className="logo">
          <img src={require("../assets/images/logo.svg")} />
        </section>
        <section className="middle">
          <Link href="/">
            <a>Home</a>
          </Link>
          <Link href="/blah">
            <a>Blah</a>
          </Link>
        </section>
        <section>
          <ProfileBadge />
        </section>
      </nav>
      <style jsx>
        {`
          div {
            background: ${PRIMARY_BG};
            color: ${PRIMARY_FG};
            padding: 10px 0;
          }
          nav {
            display: flex;
            flex-direction: row;
            align-items: center;
            margin: 0 auto;
            width: 100%;
            max-width: ${MAX_WIDTH}px;
          }
          section.logo > img {
            max-height: 40px;
          }
          a {
            color: ${PRIMARY_FG};
            font-weight: bold;
            text-decoration: none;
          }
          a:not(:first-child) {
            margin-left: 20px;
          }
          a:hover {
            text-decoration: underline;
          }
          section {
            display: flex;
            flex-direction: row;
            padding: 0px 10px;
          }
          section:first-child {
            padding-left: 0px;
          }
          section:last-child {
            padding-right: 0px;
          }
          section.middle {
            flex: 1;
          }
        `}
      </style>
    </div>
  );
};
