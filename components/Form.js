import {
  DEFAULT_BORDER,
  BORDER_RADIUS,
  DEFAULT_FONT_FAMILY
} from "../constants/Styles";
import { PRIMARY_BG, FOREGROUND } from "../constants/Colors";

export default ({ children, dark = false, ...props }) => (
  <form {...props}>
    {children}
    <style jsx>{`
      form {
        display: flex;
        flex-direction: column;
        background: ${dark ? "#2b2b28" : ""};
        padding: 20px;
        border-radius: 10px;
        color: ${dark ? "white" : FOREGROUND};
        margin: 10px;
        flex: 1;
      }
      form :global(input),
      form :global(textarea) {
        font-family: ${DEFAULT_FONT_FAMILY};
        font-weight: 200;
        border: ${DEFAULT_BORDER};
        border-radius: ${BORDER_RADIUS};
        padding: 0.8em 1em;
        background-color: #fff;
        transition: border 0.5s;
        margin-bottom: 10px;
        text-align: center;
        min-height: 40px;
        font-size: 1.5em;
        margin-bottom: 20px;
      }

      form :global(input:focus),
      form :global(textarea:focus) {
        border-color: ${FOREGROUND};
        outline: none;
      }
      form :global(label) {
        margin-bottom: 5px;
      }
      form :global(input[type="submit"]) {
        padding: 10px 40px;
        margin-top: 10px;
        background: white;
        color: black;
        border-radius: 10px;
        cursor: pointer;
        box-shadow: 0em 0em 0px rgba(0, 0, 0, 0.2);
        transition: border 0.5s;
        font-size: 1.1em;
        font-size: 30px;
      }
    `}</style>
  </form>
);
