import { Line } from "react-chartjs-2";
import { useMemo } from "react";
import getChart from "../shared/getChart";

export default ({ data }) => {
  const config = useMemo(() => {
    return getChart(data, false, true);
  }, [data]);

  return (
    <div>
      <section>
        <img src="/imgs/emoji/delighted.svg" />
        <img src="/imgs/emoji/happy.svg" />
        <img src="/imgs/emoji/ambivalent.svg" />
        <img src="/imgs/emoji/sad.svg" />
        <img src="/imgs/emoji/distraught.svg" />
      </section>
      <article>
        <Line
          data={config.data}
          height={150}
          options={{ ...config.options, maintainAspectRatio: false }}
        />
      </article>
      <style jsx>
        {`
          div {
            display: flex;
            flex-direction: row;
            max-width: 100%;
          }
          section {
            display: flex;
            flex-direction: column;
          }
          img {
            width: 36px;
            height: 36px;
            margin-bottom: 12px;
          }
          article {
            padding-top: 10px;
            flex: 1;
            margin-left: -10px;
            height: 245px;
          }
        `}
      </style>
    </div>
  );
};
