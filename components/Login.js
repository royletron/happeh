import { useContext, useState } from "react";
import UserContext from "../context/User";
import firebase from "../constants/firebase";
import "firebase/auth";
import Button from "./Button";
import Modal, { Header, Body } from "./Modal";
import Error from "./Error";

export default () => {
  const { user, showLogin, setShowLogin } = useContext(UserContext);
  const [loading, setLoading] = useState(false);
  const [error, setError] = useState();

  const act = promise => {
    setLoading(true);
    promise
      .then(() => {
        setLoading(false);
        setShowLogin(false);
      })
      .catch(err => {
        setLoading(false);
        setError(err.message);
      });
  };

  const onGoogle = () => {
    setLoading(true);
    var provider = new firebase.auth.GoogleAuthProvider();
    act(firebase.auth().signInWithPopup(provider));
  };

  const logout = () => act(firebase.auth().signOut());

  const close = () => {
    setShowLogin(false);
  };
  if (showLogin) {
    return (
      <Modal loading={loading} onClose={close}>
        <Header>Login</Header>
        <Body>
          {user ? (
            <div>
              <Button onClick={logout}>Logout</Button>
            </div>
          ) : (
            <div>
              <Button onClick={onGoogle}>Login with Google</Button>
            </div>
          )}
          <Error error={error} />
        </Body>
        <style jsx>
          {`
            div {
              margin: 10px 0;
            }
          `}
        </style>
      </Modal>
    );
  } else {
    return "";
  }
};
