import { MAX_WIDTH } from "../constants/Styles";
import { ALT_BG, ALT_FG, FOREGROUND, BACKGROUND } from "../constants/Colors";
import Link from "next/link";

export default ({}) => (
  <footer>
    <section>
      <p>
        Made by&nbsp;<a href="https://cv.royletron.dev">@royletron</a>&nbsp;in
        the dead of&nbsp;
        <img src="/imgs/emoji/moon_inverse.svg" />
      </p>
      <small>Commit {process.env.CI_COMMIT_SHORT_SHA}</small>
    </section>
    <style jsx>
      {`
        footer {
          display: flex;
          flex-direction: row;
          padding: 20px 0 10px 0;
          justify-content: center;
          background: ${FOREGROUND};
          background-blend-mode: multiply;
          color: white;
        }
        section {
          width: 100%;
          max-width: ${MAX_WIDTH}px;
          text-align: center;
        }
        p {
          font-size: 1.2em;
          display: flex;
          flex-direction: row;
          justify-content: center;
          align-items: center;
          margin: 0px;
        }
        a,
        a:visited {
          color: white;
          font-weight: bold;
        }
        img {
          max-height: 40px;
        }
      `}
    </style>
  </footer>
);
