import { useContext } from "react";
import Badge from "./Badge";
import UserContext from "../context/User";
import Spinner from "./Spinner";

export default () => {
  const { user, loading, setShowLogin } = useContext(UserContext);
  let Content;
  if (loading) {
    Content = <Spinner />;
  } else if (!user) {
    Content = <span>🕵🏻‍♂️</span>;
  } else {
    Content = <img src={user.photoURL} />;
  }
  const onClick = () => {
    setShowLogin(true);
  };
  return (
    <Badge size={40}>
      <div onClick={onClick}>{Content}</div>
      <style jsx>
        {`
          div {
            width: 100%;
            height: 100%;
            background: white;
            display: flex;
            flex-direction: column;
            align-items: center;
            justify-content: center;
          }
          div :global(span) {
            font-size: 34px;
          }
          div > :global(img) {
            max-width: 100%;
          }
        `}
      </style>
    </Badge>
  );
};
