const functions = require("firebase-functions");
const admin = require("firebase-admin");
const sendHappyCheck = require("../../shared/sendHappyCheck");
try {
  admin.initializeApp();
} catch (e) {}

exports = module.exports = functions
  .region("europe-west1")
  .firestore.document("/userMails/{id}")
  .onCreate((snapshot, context) => {
    const data = snapshot.data();
    const { id } = data;
    const code = snapshot.id;
    return admin
      .auth()
      .getUser(id)
      .then(user => {
        if (!user) {
          console.error(`Could not find user with UID=${id}`);
          return;
        }
        return sendHappyCheck(user.email, code, admin);
      });
  });
