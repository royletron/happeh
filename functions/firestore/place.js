const reverseGeocode = require("../shared/reverseGeocode");
const functions = require("firebase-functions");
const admin = require("firebase-admin");
const moment = require("moment");
try {
  admin.initializeApp();
} catch (e) {}

const parseResults = data => {
  const response = {};
  data.forEach(result => {
    [
      "postal_town",
      "administrative_area_level_1",
      "administrative_area_level_2",
      "country"
    ].forEach(level => {
      if (!response[level]) {
        const levelComponent = result.address_components.find(
          component => component.types.indexOf(level) !== -1
        );
        if (levelComponent) {
          response[level] = levelComponent.long_name;
        }
      }
    });
  });
  return response;
};

exports = module.exports = functions
  .region("europe-west1")
  .firestore.document("/happehness/{id}")
  .onCreate((snapshot, context) => {
    const data = snapshot.data();
    const { long, lat } = data;
    return reverseGeocode({
      latlng: [lat, long],
      result_type:
        "country|administrative_area_level_1|administrative_area_level_2|postal_town"
    })
      .asPromise()
      .then(data => {
        const location = parseResults(data.json.results);
        const db = admin.firestore();
        const batch = db.batch();
        const country = db.collection("countries").doc(location.country);
        batch.set(
          country.collection("countryDays").doc(moment().format("YYYY-MM-DD")),
          {
            [data.happeh]: admin.firestore.FieldValue.increment(1)
          }
        );
        return batch.commit();
        // const level_1 = db.collection("level_1")
        //   .doc(location.administrative_area_level_1)
        //   .collection("level_2")
        //   .doc(location.administrative_area_level_2)
        //   .collection("towns")
        //   .doc(location.postal_town);
        // console.log(JSON.stringify(location));
      })
      .catch(error => {
        console.error(error.message);
      });
  });
