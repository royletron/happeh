const functions = require("firebase-functions");
const admin = require("firebase-admin");
const sendWeeklyReport = require("../../shared/sendWeeklyReport");
const charting = require("../../shared/charting");
const moment = require("moment");
try {
  admin.initializeApp();
} catch (e) {}

const uuidv4 = require("uuid/v4");

const toDate = momentDate =>
  admin.firestore.Timestamp.fromDate(momentDate.toDate());

exports = module.exports = functions
  .region("europe-west1")
  .firestore.document("/userWeeklyReports/{id}")
  .onCreate(async (snapshot, context) => {
    const data = snapshot.data();
    const { id } = data;
    const weekStart = moment()
      .startOf("week")
      .subtract(1, "week");
    const user = await admin.auth().getUser(id);
    if (!user) {
      console.error(`Could not find user with UID=${id}`);
      return;
    }
    const db = admin.firestore();
    const happehDataSnaps = await db
      .collection("happehness")
      .where("id", "==", id)
      .where("timestamp", ">=", toDate(weekStart.clone()))
      .where("timestamp", "<=", toDate(weekStart.clone().add(1, "week")))
      .get();
    const happehData = happehDataSnaps.docs.map(d => d.data());
    if (happehData.length === 0) {
      console.log("NO DATA");
      return;
    }
    const chart = await charting(admin, happehData);
    var bucket = admin.storage().bucket();
    const path = `images/${uuidv4()}/${uuidv4()}.png`;
    const file = bucket.file(path);
    await file.save(chart, {
      contentType: `image/png`
    });
    await file.makePublic();
    await sendWeeklyReport(
      user.email,
      `https://storage.googleapis.com/${bucket.name}/${path}`,
      weekStart.clone().add(1, "day")
    );
    return;
  });
