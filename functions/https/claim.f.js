const functions = require("firebase-functions");
const admin = require("firebase-admin");
const moment = require("moment");
const sendConfCode = require("../shared/sendConfCode");

try {
  admin.initializeApp();
} catch (e) {}

exports = module.exports = functions
  .region("europe-west1")
  .https.onCall(async (data, context) => {
    if (!data.email) {
      throw new functions.https.HttpsError(
        "invalid-argument",
        "Please provide all arguments"
      );
    }
    if (!context.auth.token) {
      throw new functions.https.HttpsError(
        "unauthenticated",
        "Please setup a valid identifier"
      );
    }
    return admin
      .auth()
      .updateUser(context.auth.uid, { email: data.email })
      .then(() => {
        return sendConfCode(data.email, admin).then(() => ({ success: true }));
      })
      .catch(error => {
        if (error.message.indexOf("address is already in use") !== -1) {
          return sendConfCode(data.email, admin).then(() => ({
            success: true
          }));
        } else {
          throw new functions.https.HttpsError("unknown", error.message);
        }
      });
  });
