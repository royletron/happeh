const functions = require("firebase-functions");
const admin = require("firebase-admin");
const moment = require("moment");
const sendConfCode = require("../shared/sendConfCode");

try {
  admin.initializeApp();
} catch (e) {}

exports = module.exports = functions
  .region("europe-west1")
  .https.onCall(async (data, context) => {
    if (!data.email || !data.code) {
      throw new functions.https.HttpsError(
        "invalid-argument",
        "Please provide all arguments"
      );
    }
    return admin
      .firestore()
      .collection("confirmCodes")
      .where("email", "==", data.email)
      .where("code", "==", data.code)
      .limit(5)
      .get()
      .then(results => {
        if (results.docs.length === 0) {
          throw new admin.https.HttpsError("not-found", "Incorrect code");
        } else {
          return admin.auth().getUserByEmail(data.email);
        }
      })
      .then(user => {
        return admin.auth().createCustomToken(user.uid);
      });
  });
