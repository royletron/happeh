const functions = require("firebase-functions");
const admin = require("firebase-admin");
const moment = require("moment");
const reverseGeocode = require("../shared/reverseGeocode");

try {
  admin.initializeApp();
} catch (e) {}

exports = module.exports = functions
  .region("europe-west1")
  .https.onCall(async (data, context) => {
    if (data.happeh == undefined || data.hardWeek == undefined) {
      throw new functions.https.HttpsError(
        "invalid-argument",
        "Please provide all arguments"
      );
    }
    if (!context.auth.token && !data.code) {
      throw new functions.https.HttpsError(
        "unauthenticated",
        "Please setup a valid identifier"
      );
    }
    let id;
    const db = admin.firestore();
    const batch = db.batch();
    if (data.code) {
      const mailRef = db.collection("userMails").doc(data.code);
      const mail = await mailRef.get();
      if (!mail.exists) {
        throw new functions.https.HttpsError(
          "not-found",
          "You have already submitted a rating today"
        );
      }
      const mailData = mail.data();
      if (mailData.used) {
        throw new functions.https.HttpsError(
          "not-found",
          "You have already submitted a rating today"
        );
      }
      id = mailData.id;
      batch.update(mailRef, { used: true });
    } else {
      id = context.auth.token.uid;
    }
    const key = moment().format("YYYY-MM-DD-HH");
    let record = {
      happeh: data.happeh,
      hardWeek: data.hardWeek,
      thanks: data.thanks
    };
    if (data.lat && data.long) {
      const result = await reverseGeocode(data.lat, data.long);
      record = {
        ...record,
        ...result
      };
    }
    batch.set(db.collection("happehness").doc(`${id}-${key}`), {
      ...record,
      id,
      timestamp: admin.firestore.FieldValue.serverTimestamp()
    });
    await batch.commit();
  });
