const functions = require("firebase-functions");
const admin = require("firebase-admin");
const moment = require("moment");

try {
  admin.initializeApp();
} catch (e) {}

exports = module.exports = functions
  .region("europe-west1")
  .pubsub.schedule("0 * * * *")
  .onRun(context => {
    const hour = moment().hour();
    const day = moment().day();
    const db = admin.firestore();
    const batch = db.batch();
    const isWeekend = day === 6 || day === 0;
    const users = db
      .collection("userSettings")
      .where("emailTime", "==", hour)
      .where("on", "==", true)
      .where("weekends", "==", isWeekend);
    console.log(
      `Finding users for hour=${hour} and is it the weekend=${isWeekend}`
    );
    return users.get().then(userSnaps => {
      console.log(`I got ${userSnaps.docs.length} docs.`);
      userSnaps.docs.forEach(doc => {
        //this will break with more than 500
        batch.set(db.collection("userMails").doc(), {
          id: doc.id,
          used: false
        });
      });
      return batch.commit();
    });
    return null;
  });
