const functions = require("firebase-functions");
const admin = require("firebase-admin");
const moment = require("moment");

try {
  admin.initializeApp();
} catch (e) {}

exports = module.exports = functions
  .region("europe-west1")
  .pubsub.schedule("0 8 * * MON")
  .onRun(context => {
    const db = admin.firestore();
    const batch = db.batch();
    const users = db.collection("userSettings").where("on", "==", true);
    return users.get().then(userSnaps => {
      userSnaps.docs.forEach(doc => {
        //this will break with more than 500
        batch.set(db.collection("userWeeklyReports").doc(), {
          id: doc.id
        });
      });
      return batch.commit();
    });
    return null;
  });
