const mailer = require("./emailer");
const moment = require("moment");

module.exports = (email, image, week) => {
  console.log("email", image);
  return mailer.post("send", { version: "v3.1" }).request({
    Messages: [
      {
        From: {
          Email: "hi@happeh.dev",
          Name: "Happeh"
        },
        To: [
          {
            Email: email
          }
        ],
        Subject: `Happeh.dev report for week starting ${week.format(
          "dddd MMMM Do"
        )}!`,
        TemplateID: 1219485,
        TemplateLanguage: true,
        Variables: {
          reportImage: image,
          weekStart: week.format("dddd MMMM Do").toString(),
          year: week.year().toString()
        }
      }
    ]
  });
};
