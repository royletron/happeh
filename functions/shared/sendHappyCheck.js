const mailer = require("./emailer");
const moment = require("moment");

module.exports = (email, code, admin) => {
  const today = moment();
  return mailer.post("send", { version: "v3.1" }).request({
    Messages: [
      {
        From: {
          Email: "hi@happeh.dev",
          Name: "Happeh"
        },
        To: [
          {
            Email: email
          }
        ],
        Subject: `Happeh.dev check for ${today.format("dddd")}!`,
        TemplateID: 1216644,
        TemplateLanguage: true,
        Variables: {
          code,
          dayOfYear: today.dayOfYear(),
          year: today.year(),
          day: today.format("dddd")
        }
      }
    ]
  });
};
