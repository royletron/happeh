const uuidv1 = require("uuid/v1");

const mailer = require("./emailer");

module.exports = (email, admin) => {
  const db = admin.firestore();
  const code = uuidv1().substr(0, 7);
  return db
    .collection("confirmCodes")
    .doc()
    .set({
      email,
      code,
      timestamp: admin.firestore.FieldValue.serverTimestamp()
    })
    .then(() => {
      console.log("sending");
      return mailer.post("send", { version: "v3.1" }).request({
        Messages: [
          {
            From: {
              Email: "hi@happeh.dev",
              Name: "Happeh"
            },
            To: [
              {
                Email: email
              }
            ],
            TemplateID: 1214542,
            TemplateLanguage: true,
            Subject: "Happeh.dev email confirmation code",
            Variables: {
              code,
              url: "https://bbc.co.uk"
            }
          }
        ]
      });
    });
};
