const functions = require("firebase-functions");

const mailjet = require("node-mailjet").connect(
  functions.config().mp.apikey.public,
  functions.config().mp.apikey.private
);

module.exports = mailjet;
