const googleMapsClient = require("@google/maps").createClient({
  key: "AIzaSyAaNZLbfxhAzt3_tPYFdreFuHFSvlJcNXc",
  Promise: Promise
});

const parseResults = data => {
  const response = {};
  data.forEach(result => {
    [
      "postal_town",
      "administrative_area_level_1",
      "administrative_area_level_2",
      "country"
    ].forEach(level => {
      if (!response[level]) {
        const levelComponent = result.address_components.find(
          component => component.types.indexOf(level) !== -1
        );
        if (levelComponent) {
          response[level] = levelComponent.long_name;
        }
      }
    });
  });
  return response;
};

module.exports = (lat, long) => {
  return googleMapsClient
    .reverseGeocode({
      latlng: [lat, long],
      result_type:
        "country|administrative_area_level_1|administrative_area_level_2|postal_town"
    })
    .asPromise()
    .then(data => {
      return parseResults(data.json.results);
    });
};
