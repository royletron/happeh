import { useState } from "react";
import Default from "../../layouts/Default";
import { Submit } from "../../components/Button";
import Form from "../../components/Form";
import Spinner from "../../components/Spinner";
import { functions } from "../../constants/firebase";
import { useRouter } from "next/router";

export default () => {
  const [code, setCode] = useState("");
  const [loading, setLoading] = useState(false);
  const router = useRouter();
  const { query } = router;
  const changeCode = evt => setCode(evt.target.value);
  const onSubmit = evt => {
    evt.preventDefault();
    setLoading(true);
    const httpsLogin = functions.httpsCallable("httpsLogin");
    httpsLogin({
      email: query.email,
      code
    })
      .then(({ data }) => {
        return firebase.auth().signInWithCustomToken(data);
      })
      .then(() => {
        if (query.redirect) {
          router.push(query.redirect);
        } else {
          router.push("/");
        }
      })
      .catch(error => console.error(error));
  };
  return (
    <Default>
      {loading ? (
        <section>
          <Spinner />
          <p>Checking code...</p>
        </section>
      ) : (
        <section>
          <h2>Check your email for a confirmation code</h2>
          <h3>We need to make sure you are who you say you are :P</h3>
          <Form style={{ marginTop: 0 }} onSubmit={onSubmit}>
            <input
              placeholder={"Code in here"}
              value={code}
              onChange={changeCode}
            />
            <div style={{ width: "100%" }}>
              <Submit type="submit" value="Check"></Submit>
            </div>
          </Form>
        </section>
      )}
      <style jsx>
        {`
          section {
            max-width: 500px;
            width: 100%;
            display: flex;
            flex-direction: column;
            justify-content: center;
            min-height: 100%;
            text-align: center;
          }
          h2,
          h3 {
            margin-bottom: 0px;
          }
        `}
      </style>
    </Default>
  );
};
