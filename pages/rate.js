import Default from "../layouts/Default";
import Rater from "../components/Rater";

export default () => {
  return (
    <Default>
      <section>
        <Rater />
      </section>
      <style jsx>
        {`
          section {
            max-width: 500px;
            width: 100%;
            display: flex;
            flex-direction: column;
            justify-content: center;
            min-height: 100%;
            text-align: center;
          }
        `}
      </style>
    </Default>
  );
};
