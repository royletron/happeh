import React from "react";
import App from "next/app";

import firebase from "../constants/firebase";
import { UserProvider } from "../context/User";
import { UserSettingsProvider } from "../context/UserSettings";
export default class ProductivityQuotes extends App {
  constructor(props) {
    super(props);
    this.state = { hasError: false, errorCode: undefined };
  }

  static getDerivedStateFromError(error) {
    // Update state so the next render will show the fallback UI.
    console.error(error);
    return { hasError: true };
  }

  componentDidCatch(error) {
    this.setState({ errorCode: error.code });
  }

  componentDidMount() {
    firebase.analytics().logEvent("page_view");
  }

  componentDidUpdate(props) {
    firebase.analytics().logEvent("page_view");
  }

  render() {
    if (this.state.hasError) {
      // You can render any custom fallback UI
      return <h1>Something went wrong. {this.state.errorCode}</h1>;
    }
    const { Component, pageProps } = this.props;
    return (
      <UserProvider>
        <UserSettingsProvider>
          <Component {...pageProps} />
        </UserSettingsProvider>
      </UserProvider>
    );
  }
}
