import Default from "../layouts/Default";
import Button from "../components/Button";
import Link from "next/link";
import { useContext, Fragment, useEffect, useState } from "react";
import UserContext from "../context/User";
import Spinner from "../components/Spinner";
import HappehChart from "../components/LazyChart";

export default () => {
  const { user, loading } = useContext(UserContext);
  const [fakeData, setFakeData] = useState();
  useEffect(() => {
    let timeout;
    const loop = () => {
      setFakeData(new Array(7).fill().map(() => Math.floor(Math.random() * 5)));
      timeout = setTimeout(loop, 4000);
    };
    loop();
    return () => clearTimeout(timeout);
  }, [setFakeData]);
  return (
    <Default>
      <section>
        {loading ? (
          <Spinner />
        ) : (
          <Fragment>
            <h2>
              Find Time & be <b className="happeh">Happeh</b>
            </h2>
            <p>
              There's little time in the day to stop & reflect.{" "}
              <b className="happeh">Happeh</b> gives you time to stop, pause &
              reflect. It will help you unlock the key that makes you happy.
            </p>
            <div className="chart">
              <div className="button">
                <Link href="/rate" as="/rate">
                  <Button hero>Get started.</Button>
                </Link>
              </div>
              <section>
                <HappehChart data={fakeData} />
              </section>
            </div>
          </Fragment>
        )}
      </section>
      <style jsx>
        {`
          section {
            max-width: 500px;
            width: 100%;
            display: flex;
            flex-direction: column;
            justify-content: center;
            min-height: 100%;
            text-align: center;
          }

          .chart {
            position: relative;
          }

          .button {
            position: absolute;
            top: 100px;
            left: 0px;
            right: 0px;
            z-index: 10;
            text-align: center;
          }

          .chart > section {
            opacity: 0.6;
          }
        `}
      </style>
    </Default>
  );
};
