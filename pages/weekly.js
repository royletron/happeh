import Default from "../layouts/Default";
import Button from "../components/Button";
import { useContext, useMemo, Fragment } from "react";
import UserContext from "../context/User";
import { useRouter } from "next/router";
import UserSettingsContext from "../context/UserSettings";
import Spinner from "../components/Spinner";

export default () => {
  const { user, loading: userLoading } = useContext(UserContext);
  const { settings, loading: settingsLoading } = useContext(
    UserSettingsContext
  );
  const router = useRouter();
  const nextStep = () => {
    if (user.email) {
      router.push("/weekly/configure");
    } else {
      router.push("/weekly/claim");
    }
  };
  const loading = useMemo(() => userLoading || settingsLoading, [
    userLoading,
    settingsLoading
  ]);
  useMemo(() => {
    if (settings && settings.on) {
      router.push("/weekly/configure");
    }
  }, [settings]);
  return (
    <Default>
      <section>
        {loading ? (
          <Spinner />
        ) : (
          <Fragment>
            <h2>Would you like daily prompts?</h2>
            <p>
              We'll send a short daily template with the right questions that
              will allow to stop, pause & reflect. It'll only take you two
              ticks. We'll send you a weekly summary and you can find the key
              that makes you happy.
            </p>
            <div style={{ padding: 20 }}>
              <Button onClick={nextStep}>Yes please!</Button>
            </div>
            <p>
              <b>Note</b> we do need your email. This will then be assigned to
              your ID. Don't worry this is all heavily encrypted it's just so we
              email the right person the right stuff.
            </p>
          </Fragment>
        )}
      </section>
      <style jsx>
        {`
          h1 {
            text-transform: uppercase;
            font-size: 4em;
          }
          a {
            font-size: 1.2em;
            margin: 5px;
          }
          section {
            max-width: 500px;
            width: 100%;
            display: flex;
            flex-direction: column;
            justify-content: center;
            min-height: 100%;
            text-align: center;
          }
        `}
      </style>
    </Default>
  );
};
