import Default from "../layouts/Default";
import Link from "next/link";
import { useRouter } from "next/router";
import { useContext, useMemo } from "react";
import UserSettingsContext from "../context/UserSettings";
import Button from "../components/Button";

export default () => {
  const router = useRouter();
  const { settings } = useContext(UserSettingsContext);
  const alreadySetup = useMemo(() => {
    return settings && settings.on;
  }, [settings]);
  const { fromCode } = router.query;
  return (
    <Default>
      <section>
        <h2>Thanks, here's to your Happehness.</h2>
        <img src="/imgs/emoji/beer.svg" />
        <div style={{ padding: 20 }}>
          {alreadySetup ? (
            <Link href="/weekly/configure">
              <Button>Change email settings</Button>
            </Link>
          ) : (
            fromCode !== "true" && (
              <Link href="/weekly">
                <Button>Remind me tomorrow</Button>
              </Link>
            )
          )}
        </div>
        <h4>Feel free to close the browser, or...</h4>
        <Link href="/">
          <a>Head home</a>
        </Link>
      </section>
      <style jsx>
        {`
          h1 {
            text-transform: uppercase;
            font-size: 4em;
          }
          img {
            max-height: 70px;
            margin-bottom: 20px;
          }
          a {
            font-size: 1.2em;
            margin: 5px;
          }
          section {
            max-width: 500px;
            width: 100%;
            display: flex;
            flex-direction: column;
            justify-content: center;
            min-height: 100%;
            text-align: center;
          }
        `}
      </style>
    </Default>
  );
};
