import { useContext, useMemo, useState, useEffect } from "react";
import { useDocumentData } from "react-firebase-hooks/firestore";
import firebase from "../../constants/firebase";

import UserContext from "../../context/User";
import { useRouter } from "next/router";
import Default from "../../layouts/Default";
import Spinner from "../../components/Spinner";
import Form from "../../components/Form";
import Selector, { Option } from "../../components/Selector";
import Button, { Submit } from "../../components/Button";
import UserSettingsContext from "../../context/UserSettings";

const ConfirgureForm = ({
  onSave,
  settings = { on: true, emailTime: 16, weekends: false }
}) => {
  const [on, setOn] = useState(settings.on);
  const [emailTime, setEmailTime] = useState(settings.emailTime);
  const [weekends, setWeekends] = useState(settings.weekends);
  const onSubmit = evt => {
    evt.preventDefault();
    onSave(on, emailTime, weekends);
  };

  const onEmailTimeChange = evt => {
    const num = parseInt(evt.target.value);
    if (num >= 0 && num < 24) {
      setEmailTime(num);
    }
  };
  return (
    <Form onSubmit={onSubmit}>
      <label>Daily emails turned on?</label>
      <Selector value={on} onChange={setOn}>
        <Option value={true}>Yes</Option>
        <Option value={false}>No</Option>
      </Selector>
      <label>
        Hour to email (24hrs GMT,{" "}
        <small>work out your own time difference</small>)
      </label>
      <input
        onChange={onEmailTimeChange}
        value={emailTime}
        type="number"
        min="0"
        max="23"
      ></input>
      <label>Email on Saturday & Sunday?</label>
      <Selector value={weekends} onChange={setWeekends}>
        <Option value={true}>Yes</Option>
        <Option value={false}>No</Option>
      </Selector>
      <Submit value="Save" />
    </Form>
  );
};

export default () => {
  const { user, loading: userLoading } = useContext(UserContext);
  const { settings, loading: settingsLoading, onSave } = useContext(
    UserSettingsContext
  );
  const [saveState, setSaveState] = useState();

  const router = useRouter();

  const onSettingsSave = (on, emailTime, weekends) => {
    onSave(on, emailTime, weekends).then(() => {
      setSaveState(
        on
          ? `Expect an email at ${emailTime % 12}${
              emailTime / 12 > 1 ? "pm" : "am"
            } tomorrow.`
          : "You have turned emails off"
      );
    });
  };

  useMemo(() => {
    if (!userLoading) {
      if (!user || !user.email) {
        router.push("/weekly/claim");
      }
    }
  }, [userLoading, user]);

  const loading = useMemo(() => userLoading || settingsLoading, [
    userLoading,
    settingsLoading
  ]);

  const reconfigure = () => {
    setSaveState();
  };

  return (
    <Default>
      {loading ? (
        <section>
          <Spinner />
          <p>Busy...</p>
        </section>
      ) : saveState ? (
        <section>
          <img src="/imgs/emoji/tweet.svg" />
          <h2>{saveState}</h2>
          <div style={{ padding: 20 }}>
            <Button onClick={reconfigure}>Reconfigure</Button>
          </div>
        </section>
      ) : (
        <section>
          <h2>Configure your emails</h2>
          <ConfirgureForm onSave={onSettingsSave} settings={settings} />
        </section>
      )}
      <style jsx>
        {`
          img {
            max-height: 70px;
            margin-bottom: 20px;
          }
          section {
            max-width: 500px;
            width: 100%;
            display: flex;
            flex-direction: column;
            justify-content: center;
            min-height: 100%;
            text-align: center;
          }
          h2 {
            margin-bottom: 0px;
          }
        `}
      </style>
    </Default>
  );
};
