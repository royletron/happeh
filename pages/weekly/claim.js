import { useState, useContext } from "react";
import Default from "../../layouts/Default";
import { Submit } from "../../components/Button";
import Form from "../../components/Form";
import Spinner from "../../components/Spinner";
import { functions } from "../../constants/firebase";
import { useRouter } from "next/router";

export default () => {
  const [loading, setLoading] = useState(false);
  const [email, setEmail] = useState("");
  const router = useRouter();
  const onChangeEmail = evt => setEmail(evt.target.value);
  const onSubmit = evt => {
    evt.preventDefault();
    setLoading(true);
    const httpsClaim = functions.httpsCallable("httpsClaim");
    httpsClaim({ email })
      .then(data => {
        router.push({
          pathname: "/claim/confirm",
          query: {
            redirect: "/weekly/configure",
            email
          }
        });
      })
      .catch(err => {
        setLoading(false);
      });
  };
  return (
    <Default>
      {loading ? (
        <section>
          <Spinner />
          <p>Busy...</p>
        </section>
      ) : (
        <section>
          <h2>First, what is your email?</h2>
          <Form style={{ marginTop: 0 }} onSubmit={onSubmit}>
            <input value={email} onChange={onChangeEmail} />
            <div style={{ width: "100%" }}>
              <Submit type="submit" value="Next"></Submit>
            </div>
          </Form>
        </section>
      )}
      <style jsx>
        {`
          section {
            max-width: 500px;
            width: 100%;
            display: flex;
            flex-direction: column;
            justify-content: center;
            min-height: 100%;
            text-align: center;
          }
          h2 {
            margin-bottom: 0px;
          }
        `}
      </style>
    </Default>
  );
};
