import Default from "../layouts/Default";
import { useContext } from "react";
import UserContext from "../context/User";
import Link from "next/link";
import Button from "../components/Button";

export default () => {
  const { user, loading } = useContext(UserContext);
  return (
    <Default>
      <section>
        <p>
          <b>
            So my data, including my location, is encrypted and anonymised - but
            at the same time you can claim to share all data openly? WTF dude?
          </b>
        </p>
        <p>
          <b className="happeh">happeh</b> aims to demystify it's model. So here
          goee:
        </p>
        <p id="location">
          <b>Location data</b> is converted from a specific geolocation
          (longitude and latitude) into generic labels such as 'England' and
          'Leeds'. We never save your specific geolocation.
        </p>
        <p id="shared">
          <b>Sharing of data</b> will be done as a series of reports, as well as
          an interactive heatmap (<em>coming soon</em>). In future we will also
          provide an API for others to build happiness rating applications of
          their own.
        </p>
        <p>
          <b>In order to recognise you</b> as an individual. We have assigned
          you an 'ID' yours currently is{" "}
          <em>{loading ? "loading" : user ? user.uid : "waiting"}</em>. All of
          your submitted info will be marked with that ID. This isn't to track
          you as any specific person, but to mark you as an invidual. You can
          claim this ID with an email address if you would like, but only if you
          want to build your own data profile. And we <b>never</b> provide the
          data that links an ID to a given email address.
        </p>
        <div style={{ padding: 10 }}>
          <Link href="/">
            <Button>Go Back</Button>
          </Link>
        </div>
      </section>
      <style jsx>
        {`
          section {
            max-width: 500px;
            width: 100%;
            display: flex;
            flex-direction: column;
            justify-content: center;
            min-height: 100%;
            text-align: center;
          }
        `}
      </style>
    </Default>
  );
};
