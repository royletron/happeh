// copy to config/firebaseConfig.js

const firebaseConfig = {
  // copy from Firebase console e.g.
  apiKey: "whatever-whatever",
  authDomain: "whatever.firebaseapp.com",
  databaseURL: "https://whatever.firebaseio.com",
  projectId: "whatever",
  storageBucket: "whatever.appspot.com",
  messagingSenderId: "whatever",
  appId: "1:whatever:web:whatever"
};

export default firebaseConfig;
