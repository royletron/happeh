import React, { createContext, useContext, useState, useMemo } from "react";
import { useDocumentData } from "react-firebase-hooks/firestore";

import firebase from "../constants/firebase";
import "firebase/firestore";
const UserSettingsContext = createContext();

import UserContext from "./User";

export const UserSettingsProvider = ({ children }) => {
  const { user } = useContext(UserContext);
  const [triggeredLoading, setTriggeredLoading] = useState(false);

  const [settings, settingsLoading, error] = useDocumentData(
    user
      ? firebase
          .firestore()
          .collection("userSettings")
          .doc(user.uid)
      : null
  );

  const onSave = (on, emailTime, weekends) => {
    setTriggeredLoading(true);
    return firebase
      .firestore()
      .collection("userSettings")
      .doc(user.uid)
      .set({
        on,
        emailTime,
        weekends
      })
      .then(() => {
        setTriggeredLoading(false);
      });
  };

  const loading = useMemo(() => triggeredLoading || settingsLoading, [
    triggeredLoading,
    settingsLoading
  ]);

  return (
    <UserSettingsContext.Provider
      value={{
        settings,
        loading,
        onSave
      }}
    >
      {children}
    </UserSettingsContext.Provider>
  );
};

export default UserSettingsContext;
