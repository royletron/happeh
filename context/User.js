import React, { createContext, useMemo } from "react";
import { useAuthState } from "react-firebase-hooks/auth";

import firebase from "../constants/firebase";
import "firebase/auth";
import "firebase/firestore";
const UserContext = createContext();

export const UserProvider = ({ children }) => {
  const [user, loading, error] = useAuthState(firebase.auth());
  useMemo(() => {
    if (!user && !loading) {
      firebase.auth().signInAnonymously();
    }
  }, [user, loading]);
  return (
    <UserContext.Provider
      value={{
        user,
        loading
      }}
    >
      {children}
    </UserContext.Provider>
  );
};

export default UserContext;
