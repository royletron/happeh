# If you are using next

next_setup:
	yarn install

next_install:
	yarn install

next_build:
	yarn next:build
	yarn next:export

next_dev:
	LOCAL_FUNCTIONS=true yarn next:dev

# If you are using firebase

firebase_setup:
	npm install -g firebase-tools

firebase_install:
	cd functions && yarn install

firebase_build:
	echo 'Nothing to do for firebase_build'

firebase_dev:
	firebase emulators:start --only functions

.PHONY: setup
setup: next_setup

.PHONY: install
install: next_install firebase_install

.PHONY: build
build: next_build

.PHONY: dev
dev: next_dev firebase_dev