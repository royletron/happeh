import firebase from "firebase/app";
import "firebase/performance";
import "firebase/analytics";
import "firebase/functions";
import "firebase/firestore";

import firebaseConfig from "../config/firebaseConfig";

if (!firebase.apps.length) {
  firebase.initializeApp(firebaseConfig);
  if (process.browser) {
    firebase.performance();
    var db = firebase.firestore();
    var analytics = firebase.analytics();
    if (location.hostname === "localhost") {
      // Note that the Firebase Web SDK must connect to the WebChannel port
      db.settings({
        host: "localhost:8081",
        ssl: false
      });

      firebase.functions().useFunctionsEmulator("http://localhost:5001");
    }
  }
}

export default firebase;
export const functions = firebase
  .app()
  .functions(
    process.browser && location.hostname === "localhost"
      ? undefined
      : "europe-west1"
  );

if (process.browser) {
  window.firebase = firebase;
}
