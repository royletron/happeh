export const BACKGROUND = "#edf7fa";
export const FOREGROUND = "#2b2b28";
export const PRIMARY_BG = "#2b2b28";
export const PRIMARY_FG = BACKGROUND;
export const PRIMARY_GRADIENT =
  "linear-gradient(180deg, #5f6caf 0%, #3fc5f0 100%)";
export const SECONDARY_BG = "#3fc5f0";
export const SECONDARY_FG = BACKGROUND;
export const ALT_BG = "#5d5b6a";
export const ALT_GRADIENT = "linear-gradient(180deg, #5f6caf 0%, #5d5b6a 100%)";
export const ALT_FG = BACKGROUND;

export const DANGER = "#FF4136";
export const WARNING = "#FFDC00";
export const SUCCESS = "#01FF70";
export const OFF_WHITE = "#e5e5e5";
