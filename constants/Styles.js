import css from "styled-jsx/css";
import { BACKGROUND, FOREGROUND, PRIMARY_BG } from "./Colors";

export const MAX_WIDTH = 600;

export const MEDIA_MOBILE = "only screen and (max-width: 600px)";
export const SMALL_SCREEN = "only screen and (max-width: 800px)";
export const MEDIA_DESKTOP = "only screen and (min-width: 600px)";
export const MEDIA_BIG = "only screen and (min-width: 1100px)";

export const BORDER_RADIUS = "5px";
export const BORDER_COLOR = "rgba(0, 0, 0, 0.1)";
export const DEFAULT_BORDER = `3px solid ${BORDER_COLOR}`;

export const DEFAULT_FONT_FAMILY = '"Varela Round", sans-serif';
export const HEADER_FONT_FAMILY = '"Josefin Sans", sans-serif';

export const DEFAULT_STYLES = css.global`
  body {
    background: ${BACKGROUND};
    font-size: 18px;
    font-family: ${DEFAULT_FONT_FAMILY};
    font-weight: 400;
    color: ${FOREGROUND};
    padding: 0px;
    margin: 0px;
    overscroll-behavior: none;
  }
  html,
  body {
    height: 100%;
    min-height: 100%;
  }
  h1 {
    font-family: ${HEADER_FONT_FAMILY};
    font-size: 36px;
  }
  #__next {
    min-height: 100%;
    display: flex;
    flex-direction: column;
  }

  a,
  a:visited {
    color: ${FOREGROUND};
    font-weight: bold;
  }

  b {
    font-family: ${HEADER_FONT_FAMILY};
  }

  .happeh {
    font-family: ${HEADER_FONT_FAMILY};
    text-transform: uppercase;
  }
`;
