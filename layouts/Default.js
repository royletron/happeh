import { Fragment } from "react";
import { DEFAULT_STYLES, MAX_WIDTH } from "../constants/Styles";
import Meta from "../components/Meta";
import Footer from "../components/Footer";
import Background from "../components/Background";
import Logo from "../components/Logo";

export default ({ children }) => (
  <Fragment>
    <Meta />
    <Background />
    <div>
      <Logo />
      <div className="content">{children}</div>
    </div>
    <style jsx global>
      {DEFAULT_STYLES}
    </style>
    <Footer />
    <style jsx>
      {`
        div {
          display: flex;
          flex-direction: column;
          flex: 1;
          width: 100%;
          max-width: ${MAX_WIDTH}px;
          background: white;
          margin: 0 auto;
          align-items: center;
        }
        div.content {
          justify-content: center;
          padding-bottom: 40px;
        }
      `}
    </style>
  </Fragment>
);
