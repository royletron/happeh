import { DEFAULT_FONT_FAMILY, HEADER_FONT_FAMILY } from "../constants/Styles";

const scrubData = data => {
  const o = {
    Mon: [],
    Tue: [],
    Wed: [],
    Thu: [],
    Fri: [],
    Sat: [],
    Sun: []
  };

  data.forEach(item => {
    o[item.date.toDateString().subStr(0, 3)].push(item.happeh);
  });

  return Object.keys(o).map(
    key =>
      o[key].reduce((current, score) => current + (score + 2), 0) /
      o[key].length
  );
};

export default (data, scrub = true, labels = false) => {
  return {
    type: "line",
    data: {
      labels: ["Mon", "Tue", "Wed", "Thu", "Fri", "Sat", "Sun"],
      datasets: [
        {
          label: "Happehness",
          data: scrub ? scrubData(data) : data,
          borderColor: "#2B2B28",
          backgroundColor: "rgba(0,0,0,0.2)",
          pointBackgroundColor: "#2B2B28",
          pointRadius: 6
        }
      ]
    },
    options: {
      legend: {
        display: false
      },
      layout: {
        padding: {
          left: 0,
          right: 10,
          top: 10,
          bottom: 0
        }
      },
      responsive: true,
      scales: {
        xAxes: [
          {
            ticks: {
              display: labels,
              fontFamily: HEADER_FONT_FAMILY,
              fontSize: 20,
              fontColor: "#2b2b28"
            }
          }
        ],
        yAxes: [
          {
            ticks: {
              beginAtZero: true,
              display: false,
              suggestedMin: 0,
              suggestedMax: 4,
              stepSize: 1
            }
          }
        ]
      }
    }
  };
};
